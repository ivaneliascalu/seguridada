<?php

class Model_Usuario extends CI_Model
{
  function __construct()
  {
    parent:: __construct();
    $this->load->database();
  }

  public function selPerfil()
  {
    $query = $this->db->query("select * from perfil");
    return $query->result();
  }

  public function insertarUsuario(
    $idper, $DNI, $email, $contrasena, $nombres, $apellidos, $celular, $estado
  ) {
    $arrayDatos = array (
      'perid' => $idper,
      'usudni' => $DNI,
      'usuemail' => $email,
      'usucontrasenia' => ($contrasena), //md5 delante de contraseña para encriptar
      'usunombres' => $nombres,
      'usuapellidos' => $apellidos,
      'usucelular' => $celular,
      'usuestado' => $estado
    );

    $this->db->insert('usuario', $arrayDatos);
  }

  public function listarUsuario()
  {
    $query = $this->db->query("
      SELECT * FROM usuario inner join perfil on usuario.perid = perfil.perid
    ");

    return $query->result();
  }

  public function deleteUsuario($id)
  {
    $this->db->where('usuid', $id);
    $this->db->delete('usuario');
  }

  public function editUsuario($id)
  {
    $consulta = $this->db->query("
      SELECT * FROM usuario INNER JOIN perfil ON usuario.perid = perfil.perid WHERE usuario.usuid = $id
    ");

    return $consulta->result();
  }

  public function updateUsuario(
    $txtUsuid,
    $txtperf,
    $txtDNI,
    $txtemail,
    $txtcontraseña,
    $txtNombres,
    $txtApellidos,
    $txtCelular,
    $txtEstado
  ) {
    $array = array(
      'perid' => $txtperf,
      'usudni' => $txtDNI,
      'usuemail' => $txtemail,
      'usucontrasenia' => ($txtcontraseña), //md5
      'usunombres' => $txtNombres,
      'usuapellidos' => $txtApellidos,
      'usucelular' => $txtCelular,
      'usuestado' => $txtEstado
    );

    $this->db->where('usuid', $txtUsuid);
    $this->db->update('usuario', $array);
  }
}
