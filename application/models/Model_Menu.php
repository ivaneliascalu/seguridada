<?php

class Model_Menu extends CI_Model
{
  function __construct()
  {
    parent:: __construct();
    $this->load->database();
  }
  //listar menus
  public function obtenerMenus($pernombre)
  {
    $query = $this->db->query("
      SELECT
        menu.menid, menu.mensubid, menu.mennombre, menu.mencontrolador,
        menu.menaccion, menu.menicono, menu.menorden
      FROM menu
      INNER JOIN mep ON menu.menid = mep.menid
      INNER JOIN perfil ON mep.perid = perfil.perid
      WHERE perfil.pernombre = '$pernombre' AND menu.mensubid is NULL
      ORDER BY menu.menorden
    ");

    $data = array();

    foreach ($query->result() as $menu) {
      $submenus = $this->db->query("
        SELECT
          menu.menid, menu.mensubid, menu.mennombre, menu.mencontrolador,
          menu.menaccion, menu.menicono, menu.menorden
        FROM menu
        WHERE menu.mensubid = $menu->menid
        ORDER BY menu.menorden
      ");

      $data["$menu->menid"] = array(
        'mennombre' => $menu->mennombre,
        'mencontrolador' => $menu->mencontrolador,
        'menaccion' => $menu->menaccion,
        'menicono' => $menu->menicono,
        'menorden' => $menu->menorden,
        'submenus' => array()
      );

      foreach ($submenus->result() as $submenu) {
        $data["$menu->menid"]["submenus"]["$submenu->menid"] = array(
          'mennombre' => $submenu->mennombre,
          'mencontrolador' => $submenu->mencontrolador,
          'menaccion' => $submenu->menaccion,
          'menicono' => $submenu->menicono,
          'menorden' => $submenu->menorden,
        );
      }
    }

    return $data;
  }

  public function insertarMenu(
    $mennombre, $mencontrolador, $menaccion, $menicono, $menorden
  ) {
    $arrayDatos = array (
      'mennombre' => $mennombre,
      'mencontrolador' => $mencontrolador,
      'menaccion' => $menaccion,
      'menicono' => $menicono,
      'menorden' => $menorden
    );

    $this->db->insert('menu', $arrayDatos);
  }

  public function listarMenus()
  {
    $query = $this->db->query("SELECT * FROM menu WHERE menu.mensubid is NULL");
    return $query->result();
  }

  public function deleteMenu($id)
  {
    $this->db->where('menid', $id);
    $this->db->delete('menu');
  }

  public function editMenu($id)
  {
    $consulta = $this->db->query("SELECT * FROM menu WHERE menu.menid = $id");
    return $consulta->result();
  }

  public function updateMenu(
    $menid, $mennombre, $mencontrolador, $menaccion, $menicono, $menorden
  ) {
    $array = array(
      'mennombre' => $mennombre,
      'mencontrolador' => $mencontrolador,
      'menaccion' => $menaccion,
      'menicono' => $menicono,
      'menorden' => $menorden,
    );

    $this->db->where('menid', $menid);
    $this->db->update('menu', $array);
  }
}
