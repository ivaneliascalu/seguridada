<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Menus extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    //carga los menus  del usurio  por medio session
    $this->load->model('Model_Menu');
    $this->menues = $this->Model_Menu->obtenerMenus($this->session->userdata('pernombre'));
  }
}
