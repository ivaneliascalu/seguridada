<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/CI_Menus.php';

Class Usuario extends CI_Menus
{
  function __construct()
  {
    parent:: __construct();
    $this->load->model('Model_Usuario');
  }

  public function index()
  {
    $data['contenido'] = "usuario/index";
    //recuperamos los datos de perfil
    $data['selPerfil'] = $this->Model_Usuario->selPerfil();
    $data['listaUsuario'] = $this->Model_Usuario->listarUsuario();
    $this->load->view("plantilla", $data);
  }

  public function insert()
  {
    $datos = $this->input->post();

    if(isset($datos))
    {
      $txtId = $datos['txtIdperf'];
      $txtDNI = $datos['txtDNI'];
      $txtemail = $datos['txtemail'];
      $txtcontraseña = $datos['txtcontraseña'];
      $txtNombres = $datos['txtNombres'];
      $txtApellidos = $datos['txtApellidos'];
      $txtCelular = $datos['txtCelular'];
      $txtEstado = $datos['txtEstado'];

      $this->Model_Usuario->insertarUsuario(
        $txtId,
        $txtDNI,
        $txtemail,
        $txtcontraseña,
        $txtNombres,
        $txtApellidos,
        $txtCelular,
        $txtEstado
      );

      redirect('Usuario');
    }
  }

  public function delete($id = NULL)
  {
    if($id != NULL)
    {
      $this->Model_Usuario->deleteUsuario($id);
      redirect('Usuario');
    }
  }

  public function edit ($id = NULL)
  {
    if($id != NULL)
    {
      //Mostrar datos
      $data['contenido'] = 'usuario/edit';
      $data['selPerfil'] = $this->Model_Usuario->selPerfil();
      $data['datosUsuario'] = $this->Model_Usuario->editUsuario($id);
      $this->load->view('plantilla',$data);
    }
    else
    {
      //regresar a index
      redirect('');
    }
  }

  public function update ()
  {
    $datos = $this->input->post();

    if(isset($datos))
    {
      $txtUsuid = $datos['txtUsuid'];
      $txtperf = $datos['txtperf'];
      $txtDNI = $datos['txtDNI'];
      $txtemail = $datos['txtemail'];
      $txtcontraseña = $datos['txtcontraseña'];
      $txtNombres = $datos['txtNombres'];
      $txtApellidos = $datos['txtApellidos'];
      $txtCelular = $datos['txtCelular'];
      $txtEstado=$datos['txtEstado'];

      $this->Model_Usuario->updateUsuario(
        $txtUsuid,
        $txtperf,
        $txtDNI,
        $txtemail,
        $txtcontraseña,
        $txtNombres,
        $txtApellidos,
        $txtCelular,
        $txtEstado
      );

      redirect('');
    }
  }
}
