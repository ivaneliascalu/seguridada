<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/CI_Menus.php';

Class Menu extends CI_Menus
{
  function __construct()
  {
    parent:: __construct();
    $this->load->model('Model_Menu');
  }

  public function index()
  {
    // $data['contenido'] = "menu/index";
    // $data['menus'] = $this->Model_Menu->listarMenus();
    // $this->load->view("plantilla", $data);
  }

  public function insert()
  {
    $datos = $this->input->post();

    if(isset($datos))
    {
      $mennombre = $datos['mennombre'];
      $mencontrolador = $datos['mencontrolador'];
      $menaccion = $datos['menaccion'];
      $menicono = $datos['menicono'];
      $menorden = $datos['menorden'];

      $this->Model_Menu->insertarMenu(
        $mennombre,
        $mencontrolador,
        $menaccion,
        $menicono,
        $menorden
      );

      redirect('menu');
    }
  }

  public function delete($id = NULL)
  {
    if($id != NULL)
    {
      $this->Model_Menu->deleteMenu($id);
      redirect('Menu');
    }
  }
}
