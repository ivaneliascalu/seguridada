<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/CI_Menus.php';

class Dropdown extends CI_Menus
{
  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data['contenido'] = "dropdown/index";
    $this->load->view("plantilla", $data);
  }
}
