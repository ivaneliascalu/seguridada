<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Model_Login');
  }

  public function index()
  {
    $data['contenido']="login";
    $this->load->view("plantilla", $data);
  }

  public function validar()
  {
    $datos = $this->input->post();

    $email = $datos["txtemail"];
    $contrasenia = $datos["txtcontrasenia"];
    //consulta a la bd 
    $usuario = $this->Model_Login->existeUsuario($email, $contrasenia);


    if($usuario && $usuario->usuestado == 1)
    {
      $datoscomp = array(
        'usuid' => $usuario->usuid,
        'usunombres' => $usuario->usunombres,
        'usuemail' => $usuario->usuemail,
        'pernombre' => $usuario->pernombre,
        'logged_in' => TRUE
      );
      
      $this->session->set_userdata($datoscomp);
      //userdata de CI_Session
    }
    else
    {
      $messageCorrecto = "
        <div id='success' class='alert alert-danger' role='alert'>
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
          <strong>Hola $email !</strong> Error en Usuario y/o Contraseña.
        </div>
      ";

      echo $messageCorrecto;
    }
  }
}
