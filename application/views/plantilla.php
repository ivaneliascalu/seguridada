<!DOCTYPE html>
<html lang="es">
<head>
  <title>Seguridad</title>

  <link href="<?php echo base_url('public/css/bootstrap.css')?>" rel="stylesheet">
  <script src="<?php echo base_url('public/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('public/js/bootstrap.js')?>"></script>

</head>
<body>
  <?php
    if ($this->session->userdata('logged_in')) {
        $this->load->view('menus');
    }
    else{
      // $this->load->view('menus');
    }
  ?>

  <div id="container">
  <!--Aqui va el contenido de la vista-->
    <div class="col-md-10">
      <?php
        $this->load->view($contenido);
      ?>
    </div>
  </div>
</body>
</html>
