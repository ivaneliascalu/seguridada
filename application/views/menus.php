<nav class="navbar navbar-inverse">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href='<?php echo base_url("Principal");?>'>
        DESARROLLO
      </a>
    </div>
      <ul class="nav navbar-nav navbar-right">
        <?php foreach ($this->menues as $menu) { ?>
          <?php if (count($menu["submenus"]) > 0) { ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <?php echo ($menu["mennombre"]); ?>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
              <?php foreach ($menu["submenus"] as $submenu) { ?>
                <li>
                  <a href='<?php echo base_url("$submenu[mencontrolador]/$submenu[menaccion]");?>'>
                    <?php echo ($submenu["mennombre"]); ?>
                  </a>
                </li>
              <?php } ?>
              </ul>
            </li>
          <?php } else { ?>
            <li>
              <a href='<?php echo base_url("$menu[mencontrolador]/$menu[menaccion]");?>'>
                <?php echo ($menu["mennombre"]); ?>
              </a>
            </li>
          <?php } ?>
        <?php } ?>
      </ul>
      <!-- <?php var_dump($this->menues) ?> -->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
