<!--Aqui estara el crud de usuario-->

<h1>FORMULARIO- USUARIO</h1>

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
      <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
        CONSULTA
      </a>
    </li>    
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
    <!-- Tabla -->
      <table class="table table-hover">
        <thead>
          <th>ID</th>
          <th>Perfil</th>
          <th>DNI</th>
          <th>center</th>
          <th>Contrasena</th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Celular</th>
          <th>estado</th>
          <th>Acciones</th>
        </thead>
        <tbody>
          <?php foreach ($listaUsuario as $value) { ?>
          <tr>

            <td><?php echo $value->usuid; ?></td>
            <td><?php echo $value->pernombre; ?></td>
            <td><?php echo $value->usudni; ?></td>
            <td><?php echo $value->usuemail; ?></td>
            <td><?php echo $value->usucontrasenia; ?></td>
            <td><?php echo $value->usunombres; ?></td>
            <td><?php echo $value->usuapellidos; ?></td>
            <td><?php echo $value->usucelular; ?></td>
            <td><?php echo $value->usuestado; ?></td>
            <td>
              <center>
                opciones aquí
              </center>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="profile">
      <div class="row">
        <div class="col-md-7">
          <form method="POST" action="<?php echo base_url('Usuario/insert')?>">
            <div class="form-group">
              <label for="exampleInputEmail1">
                Perfil
              </label>
              <select name="txtIdperf" class="form-control">
              <?php foreach ($selPerfil as $value) { ?>
                <option value="<?php echo $value->perid?>"> <?php echo $value->pernombre; ?></option>
              <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                DNI
              </label>
              <input type="text" name="txtDNI" class="form-control" id="exampleInputEmail1" placeholder="DNI">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                EMAIL
              </label>
              <input type="text" name="txtemail" class="form-control" id="exampleInputEmail1" placeholder="Email">
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">
                  CONTRASEÑA
                </label>
                <input type="password" name="txtcontraseña" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                NOMBRES
              </label>
              <input type="text"  name="txtNombres" class="form-control" id="exampleInputEmail1" placeholder="Nombres">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                APELLIDOS
              </label>
              <input type="text" name="txtApellidos" class="form-control" id="exampleInputEmail1" placeholder="Apellidos">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                CELULAR
              </label>
              <input type="text"  name="txtCelular" class="form-control" id="exampleInputEmail1" placeholder="Celular">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                ESTADO
              </label>
              <input type="text"  name="txtEstado" class="form-control" id="exampleInputEmail1" placeholder="estado">
            </div>

            <button type="submit" class="btn btn-default">Registrar Usuario</button>
          </form>

        </div>

      </div>

    </div>

  </div>

</div>
