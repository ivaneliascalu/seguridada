<form method="POST" action="<?php echo base_url('Usuario/update')?>">
  <?php foreach($datosUsuario as $value) { ?>
  <input type="hidden" name="txtUsuid" value="<?php echo $value->usuid; ?>">
  <div class="form-group">
    <label for="exampleInputEmail1">
      Perfil
    </label>

    <?php
      $lista = array ();

      foreach ($selPerfil as $registro)
      {
        $lista[$registro->perid] = $registro->pernombre;
      }

      echo form_dropdown('txtperf', $lista, $value->perid, 'class="form-control"');
    ?>
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">
      DNI
    </label>
    <input type="text" name="txtDNI" class="form-control" id="exampleInputEmail1" value="<?php echo $value->usudni; ?>">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">
      EMAIL
    </label>
    <input type="text" name="txtemail" class="form-control" id="exampleInputEmail1" value="<?php echo $value->usuemail; ?>">
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">
      CONTRASEÑA
    </label>
    <input type="password" name="txtcontraseña" class="form-control" id="exampleInputPassword1" value="<?php echo $value->usucontrasenia; ?>">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">
      NOMBRES
    </label>
    <input type="text"  name="txtNombres" class="form-control" id="exampleInputEmail1" value="<?php echo $value->usunombres; ?>">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">
      APELLIDOS
    </label>
    <input type="text" name="txtApellidos" class="form-control" id="exampleInputEmail1" value="<?php echo $value->usuapellidos; ?>">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">
      CELULAR
    </label>
    <input type="text"  name="txtCelular" class="form-control" id="exampleInputEmail1" value="<?php echo $value->usucelular; ?>">
  </div>

  <?php }?>

  <button type="submit" class="btn btn-default">Actualizar Usuario</button>
</form>
