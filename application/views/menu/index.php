
<h1>Menus</h1>

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
      <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
        CONSULTA
      </a>
    </li>
    <li role="presentation">
      <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
        REGISTRO
      </a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
    <!-- Tabla -->
      <table class="table table-hover">
        <thead>
          <th>ID</th>
          <th>Nombre</th>
          <th>Controlador</th>
          <th>Acción</th>
          <th>Icono</th>
          <th>Orden</th>
        </thead>
        <tbody>
          <?php foreach ($menus as $value) { ?>
          <tr>

            <td><?php echo $value->menid; ?></td>
            <td><?php echo $value->mennombre; ?></td>
            <td><?php echo $value->mencontrolador; ?></td>
            <td><?php echo $value->menaccion; ?></td>
            <td><?php echo $value->menicono; ?></td>
            <td><?php echo $value->menorden; ?></td>
            <td>
              <center>
                <a href="<?php echo base_url('menu/delete')."/".$value->menid; ?>" title="Eliminar">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                  
                </a>
                <a href="<?php echo base_url('menu/edit')."/".$value->menid; ?>" title="Editar">
                  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                  
                </a>
              </center>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <div role="tabpanel" class="tab-pane" id="profile">
      <div class="row">
        <div class="col-md-7">
          <form method="POST" action="<?php echo base_url('menu/insert')?>">

            <div class="form-group">
              <label for="mennombre">
                Nombre
              </label>
              <input type="text" name="mennombre" class="form-control" id="exampleInputEmail1" placeholder="Nombre">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                 Controlador
              </label>
              <input type="text" name="mencontrolador" class="form-control" id="exampleInputEmail1" placeholder="Controlador">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                Acción
              </label>
              <select name="menaccion" class="form-control">
                <option value="index">Index</option>
                <option value="list">List</option>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                 Icono
              </label>
              <input type="text" name="menicono" class="form-control" id="exampleInputEmail1" placeholder="Icono">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">
                 Icono
              </label>
              <input type="number" name="menorden" class="form-control" id="exampleInputEmail1" placeholder="Orden">
            </div>

            <button type="submit" class="btn btn-default">Registrar Menu</button>
          </form>

        </div>

      </div>

    </div>

  </div>

</div>
