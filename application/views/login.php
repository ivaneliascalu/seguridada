<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login </title>

    <link href="<?php echo base_url('public/css/bootstrap.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('public/js/jquery.min.js')?>"></script>
    <script src="<?php echo base_url('public/js/bootstrap.js')?>"></script>

    <script>
      $(document).ready(function (e) {
        $("#form-login").on('submit', (function(e) {
          e.preventDefault();

          $("#message").empty();
          $('#loading').show();

          $.ajax({
            url: "<?php echo base_url('login/validar')?>",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            
            success: function(data) {
              
              if (data.length !== 0) {
                $('#loading').show();
                $("#message").html(data);//lo que retorna de login/validar
              } 
              else {
                window.location.href='<?php echo base_url("Principal") ?>';
                // throw new Error('go');
              }
              console.log(data);
            }
          });
        }));
      });
    </script>
    <style>
      .login-box {
        box-shadow: 0 2px 10px rgba(0,0,0,0.1);
        border-radius: 5px;
        border: 1px solid gray;
        padding: 8px;
        width: 420px;
        height: 320px;
      }
    </style>
  </head>
  <body>
      <h3 class=".text-primary"></h3>

    
      <form id="form-login" action="" method="post">
        <div class="col-lg-12">
          <label for="">
            Email
          </label>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="glyphicon glyphicon-user"></i>
            </span>
            <input name="txtemail" id="email" type="text" class="form-control" name="email" placeholder="Email">
          </div>
          <br>
          <label for="">
            Password
          </label>
          <br>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="glyphicon glyphicon-lock"></i>
            </span>
            <input name="txtcontrasenia" id="password" type="password" class="form-control" name="password" placeholder="Password">
          </div>
          <br>
          <button type="submit" class="btn btn-danger">
            Ingresar
          </button>
        </div>
      </form>
    

    <div id="message">
    </div>

  </body>
</html>
